
import java.util.*;


//LOVING THE ARRAYLIST PRE-IMPLIMENTED METHODS!
public class Symbol_table 
{
	Vector <ArrayList<Token>> stack = new Vector <ArrayList<Token>>();
	ArrayList<Token> tmp = new ArrayList<Token>();
	Vector <ArrayList<String>> nameStack = new Vector <ArrayList<String>>();
	
	void newBlock() 
	{
		stack.add(0,new ArrayList<Token>());
	//only entered when going into a new block
		for(Token t : tmp)
		{
			stack.elementAt(0).add(new Token(t.kind,t.string,t.lineNumber));
		}
		//need to push a new array onto the stack
		tmp.clear();
		//tmp.clear();//empties the temp array
	}

	void endBlock() 
	{
	//entered at end of block
		if(!stack.isEmpty())
		{
			stack.remove(0);
		}
	}

	public void declareID(Token token)
	{
	//called for declaration
			if (contains(token,tmp)) //check if id exists
				System.err.println( "variable " + token.string+ " is redeclared on line " + token.lineNumber );
			else
			{
				tmp.add(token);//if not then save in end of array
			}
	}

	public void checkID(Token token) 
	{
	//called if assignment
		//System.out.println("Top of stack: "+ stack);
		//System.out.println("Token: " + token);
		if(!stack.isEmpty())
		{
			boolean found = false;
			for(ArrayList<Token> list: stack)
			{
				if (contains(token,list)) //check if id exists
				{	
					found = true;
					break;
				}
			}
			if(!found)
			{
				System.err.println( "undeclared variable " +token.string + " on line " + token.lineNumber );//if not then error
				System.exit(1);
			}
		}
		else
		{
			System.err.println( "undeclared variable " +token.string + " on line " + token.lineNumber );//if not then error
			System.exit(1);
		}
	}
	
	public boolean contains(Token token, ArrayList<Token> list)
	{
		for(Iterator<Token> i = list.iterator(); i.hasNext();)
		{
			Token t = i.next();
			if(t.string.equals(token.string))
			{			
				return true;
			}
		}
		return false;
		
	}
}