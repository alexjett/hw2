public class Parser {
Scan scanner;
Symbol_table st = new Symbol_table();
CodeGenerator gc = new CodeGenerator();
Token token;//Current token we are using
	public Parser(Scan scanner) {
		this.scanner=scanner;
		token = scanner.scan();//First token in file
		program();//Starts the whole grammar check
		if(compare(TK.ERROR))
			error("junk after logical end of program");
		
	}
	
	void program()
	{
		gc.header(); 
		block();
		st.print();
		gc.end();
		gc.print();
	}
	void block()
	{
		
		if(isStatement())
			statement_list();
		else if(compare(TK.VAR))
		{
			declarations();
			if(!isStatement())
				error("junk after logical end of program");
			else 
				statement_list();
			st.endBlock();
		}
		else if(compare(TK.EOF)){
			//System.out.println("error called in BLOCK with token: "+ token.string);//debugger-----
			error();
		}
		else
			error("junk after logical end of program");		
	}

	
	void declarations()
	{
		mustbe(TK.VAR);
		gc.codegen(token);
		next();
		while(compare(TK.ID))
		{
			st.declareID(token);
			gc.codegen(token);
			next();
			if (compare(TK.ID))
				gc.comma();
		}
		mustbe(TK.RAV);
		gc.codegen(token);
		st.newBlock();
		next();
	}

	void statement_list()
	{
		while(isStatement())
		{
			statement();
		}
	}
	
	void statement()
	{
		if(compare(TK.ID))
			assignment();
		else if(compare(TK.PRINT))
			print();
		else if(compare(TK.IF))
			if_stmt();
		else if(compare(TK.DO))
			do_stmt();
		else if(compare(TK.FA))
			fa();
		else 
		{
			//System.out.println("error called in STATEMENT with token: "+ token.string);//debugger--------
			error();
		}
	}
	void assignment()
	{
		mustbe(TK.ID);
		gc.codegen(token);
		st.checkID(token, true);
		next();
		mustbe(TK.ASSIGN);
		gc.codegen(token);
		next();
		expression();
		gc.semicolon();
	}
	void print()
	{
		mustbe(TK.PRINT);
		gc.codegen(token);
		next();
		expression();
		gc.code.add(")");
		gc.semicolon();
	}
	void if_stmt()
	{
		mustbe(TK.IF);
		gc.codegen(token);
		next();
		st.depth++;
		st.newBlock();
		guarded_commands();
		mustbe(TK.FI);
		gc.codegen(token);
		st.depth--;
		st.endBlock();
		next();
	}
	void do_stmt() // fuck shit **********************
	{
		mustbe(TK.DO);
		gc.codegen(token);
		next();
		st.depth++;
		st.newBlock();
		guarded_commands();
		mustbe(TK.OD);
		gc.codegen(token);// will have to change drasticly
		st.endBlock();
		st.depth--;
		next();
	}
	void fa()
	{
		mustbe(TK.FA);
		gc.codegen(token);
		st.depth++;
		st.newBlock();
		next();
		mustbe(TK.ID); //this is where shit get hard
		st.checkID(token, true);
		next();
		mustbe(TK.ASSIGN);
		gc.codegen(token);
		next();
		expression();
		mustbe(TK.TO); //also when shit gets hard
		next();
		expression();
		if(compare(TK.ST)) //also whn shit gets hard
		{
			next();
			expression();
		}	
		commands();
	}
	void guarded_commands()
	{
		guarded_command();
		while(compare(TK.BOX))
		{
			mustbe(TK.BOX);
			gc.codegen(token); //fuuuuuuuck 
			next();
			guarded_command();
		}
		if(compare(TK.ELSE)){
			gc.codegen(token);
			next();		//fixed error on t05
			commands();
		}		
	}
	void guarded_command()
	{
		expression();
		commands();
	}
	void commands()
	{
		if(compare(TK.ARROW))
		{
			gc.codegen(token);
			next();
			block();
		}
		else{
			//System.out.println("error called in COMMANDS with token: "+ token.string);//debugger-----
			error();
		}
	}
	void expression()
	{
		simple();
		if(isRelop())
		{
			gc.codegen(token);
			relop();
			simple();
		}
		
	}
	void simple()
	{
		term();
		while(compare(TK.PLUS)||compare(TK.MINUS))
		{
			mustbe(TK.PLUS,TK.MINUS);
			gc.codegen(token);
			addop();
			term();
		}	
	}
	void term()
	{
		factor();
		while(compare(TK.TIMES)||compare(TK.DIVIDE))
		{
			mustbe(TK.TIMES,TK.DIVIDE);
			gc.codegen(token);
			multop();
			factor();
		}
	}
	void factor()
	{
		// System.out.println("line "+token.lineNumber+ " " +token);
		if(compare(TK.LPAREN))
		{
			gc.codegen(token);
			next();
			expression();
			mustbe(TK.RPAREN);
			gc.codegen(token);
			next();
		}
		else if(compare(TK.ID))
		{
			gc.codegen(token);
			st.checkID(token, false);
			next();
		}
		else if(compare(TK.NUM))
		{
			gc.codegen(token);
			next();
		}
		else
			error("factor");
	}
	void relop()
	{
		next();
	}
	void addop()
	{
		next();
	}
	void multop()
	{
		next();
	}
	boolean isStatement()
	{
		
		if(compare(TK.ID))
			return true;
		if(compare(TK.DO))
			return true;
		if(compare(TK.IF))
			return true;
		if(compare(TK.PRINT))
			return true;
		if(compare(TK.FA))
			return true;
		if(compare(TK.AF))
		{
			next();
			gc.codegen(token);
			st.depth--;
			st.endBlock();
			return isStatement();
		}
		return false;
	}//Returns if the current token is the start of a statement
	boolean isRelop()
	{
		if(compare(TK.EQ))
			return true;
		if(compare(TK.LT))
			return true;
		if(compare(TK.GT))
			return true;
		if(compare(TK.NE))
			return true;
		if(compare(TK.LE))
			return true;
		if(compare(TK.GE))
			return true;
		return false;
	}
	void error()
	{
		//System.err.println("exited program");//debugger-----------
		System.exit(1);
	}//prints out an error line and exits the program
	
	void error(String s)
	{
		System.err.println( "can't parse: line "
                + token.lineNumber + " " + s );
		System.exit(1);
	}
	void next()
	{
		token = scanner.scan();
	}//Sets the token equal to the next token
	void mustbe(TK tk) {
		if( !compare(tk))
		{
			System.err.println("mustbe : want " + tk + ", got " +token);
			error("missing token (mustbe)");
		}
		//next();
	}
	void mustbe(TK tk1,TK tk2)
	{
		if( !(compare(tk1)||compare(tk2)))
		{
			System.err.println("mustbe : want " + tk1 + " or "+ tk2 + ", got " +token);
			error("missing token (mustbe)");
		}
	}
	boolean compare(TK tk)
	{
		return token.kind == tk;
	}//Returns if the current token's kind is equal to the peramater
}