import java.util.*;

public class Var 
{
	public int depth;
	public Token token;
	public ArrayList<Integer> assigned;
	public ArrayList<Integer> used;
	
	Var(Token t, int d)
	{
		used  = new ArrayList<Integer>();
		assigned  = new ArrayList<Integer>();
		token = t;
		depth = d;
	}
}
