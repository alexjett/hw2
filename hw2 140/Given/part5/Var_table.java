import java.util.*;

public class Var_table 
{
	public ArrayList<Var> table = new ArrayList<Var>();
	
	void addVar(Token tok, int d)
	{
		table.add(new Var(tok, d));
		//System.out.println("adding token: "+tok.string);//debugger
	}
	
	void assigned(Token tok, int d)
	{
		Boolean found = false;
		//need to search through table to find matching tok using d and id name
		for(int j = table.size()-1; j >=0 && !found;j--)
		{
			Var v = table.get(j);
			
			for(int i = d; i >= 0  ; i--)
			{
				if(v.token.string.equals(tok.string) && v.depth == i)
				{
					v.assigned.add(tok.lineNumber);
					found = true;
					break;
				}
			}
		}
	}
	
	void used(Token tok, int d)
	{
		Boolean found = false;
		for(int j = table.size()-1; j >=0 &&!found;j--)
		{
			Var v = table.get(j);
			
			for(int i = d; i >= 0; i--)
			{
				if(v.token.string.equals(tok.string) && v.depth == i)
				{
					v.used.add(tok.lineNumber);
					found = true;
					break;
				}
			}
		}
		
	}
	
	void print()
	{
		for(Var v: table)
		{
			System.out.println(v.token.string);
			System.out.println("  declared on line "+ v.token.lineNumber + " at nesting depth "+ v.depth);
			if(!v.assigned.isEmpty()){//---------print assigned----------
				System.out.print("  assigned to on: ");
				for(int i=0; i < v.assigned.size(); i++){	
					int count = 1;
					if(i<v.assigned.size()-1){
						while(v.assigned.get(i+1) == v.assigned.get(i)){
							count++;
							i++;
							if(i>=v.assigned.size()-1)
								break;
						}//end while
					}//end if
					System.out.print(v.assigned.get(i));
					if(count != 1)
						System.out.print("("+ count+ ")");
					if(i!=v.assigned.size()-1){
						System.out.print(" ");
					}
				}//end for
			}//end if not empty
			else //if assigned is empty
				System.out.print("  never assigned");
			System.out.println();
			
			if(!v.used.isEmpty()){//---------print used----------
				System.out.print("  used on: ");
				for(int i=0; i < v.used.size(); i++)
				{
					int count = 1;
					if(i<v.used.size()-1)
					{
						while(v.used.get(i+1) == v.used.get(i))
						{
							count++;
							i++;
							if(i>=v.used.size()-1)
								break;
						}
					}
					System.out.print(v.used.get(i));
					if(count != 1)
						System.out.print("("+ count+ ")");
					if(i!=v.used.size()-1)
					{
						System.out.print(" ");
					}
				}		
			}//end if not empty
			else
				System.out.print("  never used");
			System.out.println();		
		}
	}
}