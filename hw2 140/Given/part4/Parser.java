public class Parser {
Scan scanner;
Symbol_table st = new Symbol_table();
Token token;//Current token we are using
	public Parser(Scan scanner) {
		this.scanner=scanner;
		token = scanner.scan();//First token in file
		program();//Starts the whole grammar check
		if(compare(TK.ERROR))
			error("junk after logical end of program");
		
	}
	
	void program()
	{
		block();
		st.print();
	}
	void block()
	{
		
		if(isStatement())
			statement_list();
		else if(compare(TK.VAR))
		{
			declarations();
			if(!isStatement())
				error("junk after logical end of program");
			else 
				statement_list();
			//System.out.print(token.lineNumber+" ");//debugger
			st.endBlock();
		}
		else if(compare(TK.EOF)){
			//System.out.println("error called in BLOCK with token: "+ token.string);//debugger-----
			error();
		}
		else
			error("junk after logical end of program");		
	}

	
	void declarations()
	{
		mustbe(TK.VAR);
		next();
		while(compare(TK.ID))
		{
			st.declareID(token);
			next();
		}
		mustbe(TK.RAV);
		st.newBlock();
		next();
	}

	void statement_list()
	{
		while(isStatement())
		{
			statement();
		}
	}
	
	void statement()
	{
		if(compare(TK.ID))
			assignment();
		else if(compare(TK.PRINT))
			print();
		else if(compare(TK.IF))
			if_stmt();
		else if(compare(TK.DO))
			do_stmt();
		else if(compare(TK.FA))
			fa();
		else 
		{
			//System.out.println("error called in STATEMENT with token: "+ token.string);//debugger--------
			error();
		}
	}
	void assignment()
	{
		mustbe(TK.ID);
		st.checkID(token, true);
		next();
		mustbe(TK.ASSIGN);
		next();
		expression();
	}
	void print()
	{
		mustbe(TK.PRINT);
		next();
		expression();
	}
	void if_stmt()
	{
		mustbe(TK.IF);
		next();
		st.depth++;
		st.newBlock();
		guarded_commands();
		mustbe(TK.FI);
		//System.out.print(token.lineNumber+" ");//debugger
		st.endBlock();
		st.depth--;
		next();
	}
	void do_stmt()
	{
		mustbe(TK.DO);
		next();
		st.depth++;
		st.newBlock();
		guarded_commands();
		mustbe(TK.OD);
		//System.out.print(token.lineNumber+" ");//debugger
		st.endBlock();
		st.depth--;
		next();
	}
	void fa()
	{
		mustbe(TK.FA);
		st.depth++;
		st.newBlock();
		next();
		mustbe(TK.ID);
		st.checkID(token, true);
		next();
		mustbe(TK.ASSIGN);
		next();
		expression();
		mustbe(TK.TO);
		next();
		expression();
		if(compare(TK.ST))
		{
			next();
			expression();
		}	
		commands();
	}
	void guarded_commands()
	{
		guarded_command();
		while(compare(TK.BOX))
		{
			mustbe(TK.BOX);
			next();
			guarded_command();
		}
		if(compare(TK.ELSE)){
			next();		//fixed error on t05
			commands();
		}		
	}
	void guarded_command()
	{
		expression();
		commands();
	}
	void commands()
	{
		if(compare(TK.ARROW))
		{
			next();
			block();
		}
		else{
			//System.out.println("error called in COMMANDS with token: "+ token.string);//debugger-----
			error();
		}
	}
	void expression()
	{
		simple();
		if(isRelop())
		{
			relop();
			simple();
		}
		
	}
	void simple()
	{
		term();
		while(compare(TK.PLUS)||compare(TK.MINUS))
		{
			mustbe(TK.PLUS,TK.MINUS);
			addop();
			term();
		}	
	}
	void term()
	{
		factor();
		while(compare(TK.TIMES)||compare(TK.DIVIDE))
		{
			mustbe(TK.TIMES,TK.DIVIDE);
			multop();
			factor();
		}
	}
	void factor()
	{
		// System.out.println("line "+token.lineNumber+ " " +token);
		if(compare(TK.LPAREN))
		{
			next();
			expression();
			mustbe(TK.RPAREN);
			next();
		}
		else if(compare(TK.ID))
		{
			st.checkID(token, false);
			next();
		}
		else if(compare(TK.NUM))
			next();
		else
			error("factor");
	}
	void relop()
	{
		next();
	}
	void addop()
	{
		next();
	}
	void multop()
	{
		next();
	}
	boolean isStatement()
	{
		
		if(compare(TK.ID))
			return true;
		if(compare(TK.DO))
			return true;
		if(compare(TK.IF))
			return true;
		if(compare(TK.PRINT))
			return true;
		if(compare(TK.FA))
			return true;
		if(compare(TK.AF))
		{
			next();
			st.depth--;
			//System.out.print(token.lineNumber+" ");//debugger
			st.endBlock();
			return isStatement();
		}
		return false;
	}//Returns if the current token is the start of a statement
	boolean isRelop()
	{
		if(compare(TK.EQ))
			return true;
		if(compare(TK.LT))
			return true;
		if(compare(TK.GT))
			return true;
		if(compare(TK.NE))
			return true;
		if(compare(TK.LE))
			return true;
		if(compare(TK.GE))
			return true;
		return false;
	}
	void error()
	{
		//System.err.println("exited program");//debugger-----------
		System.exit(1);
	}//prints out an error line and exits the program
	
	void error(String s)
	{
		System.err.println( "can't parse: line "
                + token.lineNumber + " " + s );
		System.exit(1);
	}
	void next()
	{
		token = scanner.scan();
	}//Sets the token equal to the next token
	void mustbe(TK tk) {
		if( !compare(tk))
		{
			System.err.println("mustbe : want " + tk + ", got " +token);
			error("missing token (mustbe)");
		}
		//next();
	}
	void mustbe(TK tk1,TK tk2)
	{
		if( !(compare(tk1)||compare(tk2)))
		{
			System.err.println("mustbe : want " + tk1 + " or "+ tk2 + ", got " +token);
			error("missing token (mustbe)");
		}
	}
	boolean compare(TK tk)
	{
		return token.kind == tk;
	}//Returns if the current token's kind is equal to the peramater
}