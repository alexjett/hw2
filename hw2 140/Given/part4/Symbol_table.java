import java.util.*;

public class Symbol_table 
{
	Vector <ArrayList<Token>> stack = new Vector <ArrayList<Token>>();
	ArrayList<Token> tmp = new ArrayList<Token>();
	Vector <ArrayList<String>> nameStack = new Vector <ArrayList<String>>();
	public int depth = 0;
	Var_table vt = new Var_table();
	
	void newBlock() 
	{
		stack.add(0,new ArrayList<Token>());
	//only entered when going into a new block
		for(Token t : tmp)
		{
			stack.elementAt(0).add(new Token(t.kind,t.string,t.lineNumber));
		}
		//need to push a new array onto the stack
		tmp.clear();
		//tmp.clear();//empties the temp array
	}

	void endBlock() 
	{
	//entered at end of block
		/*for(Token t: stack.elementAt(0))
			System.out.println(t.string+" "+t.lineNumber+"   ");//debugger
		System.out.println(" ");//debugger
		*/
		if(!stack.isEmpty())
		{
			for(Var v: vt.table)
			{
				if(v.depth == depth&&depth!=0)
				{
					v.exists = false;
				}
			}
			stack.remove(0);
		}
	}

	public void declareID(Token token)
	{
	//called for declaration
	//System.out.println("declaring "+ token.string);//debugger
			if (contains(token,tmp)) //check if id exists
				System.err.println( "variable " + token.string+ " is redeclared on line " + token.lineNumber );
			else
			{
				tmp.add(token);//if not then save in end of array
				vt.addVar(token, depth);
			}
	}

	public void checkID(Token token, boolean assigned) 
	{
	//called if assignment
		//System.out.println("Top of stack: "+ stack);
		//System.out.println("Token: " + token);
		if(!stack.isEmpty())
		{
			boolean found = false;
			for(ArrayList<Token> list: stack)
			{
				if (contains(token,list)) //check if id exists
				{	
					found = true;
					//System.out.println("using or assigning "+ token.string);//debugger-------
					if(assigned)
						vt.assigned(token, depth);
					else
						vt.used(token, depth);
					break;
				}
			}
			if(!found)
			{
				System.err.println( "undeclared variable " +token.string + " on line " + token.lineNumber );//if not then error
				System.exit(1);
			}
		}
		else
		{
			System.err.println( "undeclared variable " +token.string + " on line " + token.lineNumber );//if not then error
			System.exit(1);
		}
	}
	
	public boolean contains(Token token, ArrayList<Token> list)
	{
		for(Iterator<Token> i = list.iterator(); i.hasNext();)
		{
			Token t = i.next();
			if(t.string.equals(token.string))
			{			
				return true;
			}
		}
		return false;	
	}
	
	public void print()
	{
		vt.print();
	}
	
}		
